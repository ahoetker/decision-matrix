This is a very simple script, minimal documentation. 

# Demo

[![asciicast](https://asciinema.org/a/x9CA0dCbByITXzIW2n5T7SW6B.svg)](https://asciinema.org/a/x9CA0dCbByITXzIW2n5T7SW6B)

# Usage

## Define criteria and designs

The criteria to be used in the AHP and decision matrix are defined in `criteria_designs.yml`. The designs are also defined in this file, and given a normalized 1-10 score for each criterion. 

## Assign relative weights between criteria

This part is interactive. Simply run `design_matrices.py`, and the prompt will ask you to rank the criteria given in `criteria_designs.yml`. 

## Include decision matrices in documents

By default, the scrpt produces `ahp.tex` and `decision_matrix.tex` to be included in a LaTeX document. 

