from pathlib import Path as p
from typing import List, Dict
from yaml import load, Loader
import pandas as pd


pd.set_option("display.precision", 2)


def load_data(data_file: p) -> Dict:
    with data_file.open("r") as f:
        data = load(f.read(), Loader=Loader)
    return data


def ask_relative_weight(criterion_a: str, criterion_b: str) -> int:
    query = f"Relative weight: {criterion_a} vs {criterion_b} = "
    return int(input(query))


def make_ahp(criteria: List[str]) -> pd.DataFrame:
    index = criteria + ["Total"]
    columns = criteria + ["Total", "WF"]
    df = pd.DataFrame(index=index, columns=columns)
    for a in range(len(criteria)):
        criterion_a = criteria[a]
        df[criterion_a][criterion_a] = 1
        for b in range(a + 1, len(criteria)):
            criterion_b = criteria[b]
            relative_weight = ask_relative_weight(criterion_a, criterion_b)
            df[criterion_b][criterion_a] = relative_weight
            df[criterion_a][criterion_b] = 1 / relative_weight
    for criterion in criteria:
        df["Total"][criterion] = df.loc[criterion].sum()
    overall_total = df["Total"].sum()
    for criterion in criteria:
        df["WF"][criterion] = df["Total"][criterion] / overall_total
    df["Total"]["Total"] = df["Total"].sum()
    df["WF"]["Total"] = df["WF"].sum()
    return df


def make_decision_matrix(
    criteria: List[str], ahp: pd.DataFrame, designs: List[Dict[str, int]]
) -> pd.DataFrame:
    index = criteria + ["Total"]
    columns = ["WF"] + [design for design in designs]
    df = pd.DataFrame(index=index, columns=columns)
    for criterion in criteria:
        df["WF"][criterion] = ahp["WF"][criterion]
        for design in designs:
            df[design][criterion] = designs[design][criterion]
    for design in designs:
        df[design]["Total"] = sum(
            [df["WF"][criterion] * df[design][criterion] for criterion in criteria]
        )
    return df


def to_latex(df: pd.DataFrame, destination: p) -> None:
    df.to_latex(str(destination), na_rep="", bold_rows=True)


if __name__ == "__main__":
    data = load_data(p("criteria_designs.yml"))
    criteria = data["criteria"]
    ahp = make_ahp(criteria)
    decision_matrix = make_decision_matrix(criteria, ahp, data["designs"])
    print(ahp)
    print(decision_matrix)
    to_latex(ahp, p("ahp.tex"))
    to_latex(decision_matrix, p("decision_matrix.tex"))
    ahp.to_html("ahp.html", na_rep="")
    decision_matrix.to_html("decision_matrix.html", na_rep="")

